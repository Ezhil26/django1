from django.urls import path,include
from .views import GenericAPIView,GenericViewset,ArticleViewset,article_list,article_detail,ArticleAPIView,Articledetail
from rest_framework.routers import DefaultRouter
#The conventions for wiring up resources into views and urls can be handled automatically, using a Router class
router=DefaultRouter()
router.register('article',ArticleViewset,basename='article')
router.register('generic',GenericViewset,basename='generic')
urlpatterns = [
    path('viewset/generic',include(router.urls)),
    path('viewset/',include(router.urls)),
path('viewset/<int:pk>/',include(router.urls)),
#path('article/',article_list ),
#path('article/',ArticleAPIView.as_view() ),
path('generic/article/<int:id>',GenericAPIView.as_view() ),
#path('detail/<int:pk>/',article_detail ),
path('detail/<int:id>/',Articledetail.as_view() ),
]